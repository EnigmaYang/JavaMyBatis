package cn.yawei.test;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cn.yawei.mappers.StudentMapper;
import cn.yawei.model.Student;
import cn.yawei.util.MyBatisUtils;

public class StudentTest {
	private static final Logger LOGGER = Logger.getLogger(TestHelloWorld.class);
	private SqlSession sqlSession = null;
	private StudentMapper studentMapper = null;

	@Before
	public void init() {
		sqlSession = MyBatisUtils.openSession();
		studentMapper = sqlSession.getMapper(StudentMapper.class);
	}

	@After
	public void destory() {
		if (sqlSession != null)
			sqlSession.close();
	}

	@Test
	public void add() {
		LOGGER.info("添加学生");
		Student student = new Student("sigma", 10);
		studentMapper.add(student);
		sqlSession.commit();
	}

	@Test
	public void update() {
		LOGGER.info("修改学生");
		Student student = new Student("xxx", 100);
		student.setId(1);
		studentMapper.update(student);
		sqlSession.commit();
	}

	@Test
	public void delete() {
		LOGGER.info("删除学生");
		studentMapper.delete(5);
		sqlSession.commit();
	}

	@Test
	public void find() {
		LOGGER.info("查找学生");
		Student student = studentMapper.findById(10);
		System.out.println(student.getName());
		System.out.println(student.getAge());
		sqlSession.commit();
	}

	@Test
	public void findAll() {
		LOGGER.info("查找全部学生");
		List<Student> studentList = studentMapper.findAll();
		for (Student student : studentList) {
			System.out.println(student.getName());
			System.out.println(student.getAge());
		}
		sqlSession.commit();
	}
}
