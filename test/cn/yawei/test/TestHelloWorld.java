package cn.yawei.test;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import cn.yawei.mappers.StudentMapper;
import cn.yawei.model.Student;
import cn.yawei.util.MyBatisUtils;

public class TestHelloWorld {

	private static final Logger LOGGER = Logger.getLogger(TestHelloWorld.class);

	public static void main(String[] args) {
		SqlSession sqlSession = MyBatisUtils.openSession();
		StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
		int count = studentMapper.add(new Student("yawei", 25));
		sqlSession.commit();
		if (count > 0) {
			LOGGER.info("添加成功 " + count);
			LOGGER.debug("添加成功 debug" + count);
		}

	}
}
