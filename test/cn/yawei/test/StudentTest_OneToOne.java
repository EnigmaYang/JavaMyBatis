package cn.yawei.test;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cn.yawei.mappers.StudentMapper;
import cn.yawei.model.Student;
import cn.yawei.util.MyBatisUtils;

public class StudentTest_OneToOne {
	private static final Logger LOGGER = Logger.getLogger(TestHelloWorld.class);
	private SqlSession sqlSession = null;
	private StudentMapper studentMapper = null;

	@Before
	public void init() {
		sqlSession = MyBatisUtils.openSession();
		studentMapper = sqlSession.getMapper(StudentMapper.class);
	}

	@After
	public void destory() {
		if (sqlSession != null)
			sqlSession.close();
	}

	@Test
	public void findByIdWithAddress() {
		LOGGER.info("����ѧ��");
		Student student = studentMapper.findByIdWithAddress(7);
		System.out.println(student.getName());
		System.out.println(student.getAge());
		System.out.println(student.getAddress().getState());
		System.out.println(student.getAddress().getCity());
		System.out.println(student.getAddress().getAddress());

		sqlSession.commit();
	}
}
