package cn.yawei.mappers;

import cn.yawei.model.Student;

public interface AddressMapper {
	public Student findById(int id);
}
