package cn.yawei.mappers;

import java.util.List;

import cn.yawei.model.Student;

public interface StudentMapper {
	public int add(Student student);

	public int update(Student student);

	public int delete(int id);

	public Student findById(int id);

	public Student findByIdWithAddress(int id);

	public List<Student> findAll();

}
